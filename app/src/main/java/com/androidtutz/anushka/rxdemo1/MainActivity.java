package com.androidtutz.anushka.rxdemo1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String myApp = "MyApp";

    private final static String TAG = "myApp";
    private String greeting = "Hello From RxJava";

    private Observable<String> myObservable;
    private DisposableObserver<String> myObserver;
    private DisposableObserver<String> myObserver1;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    //private Disposable disposable;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.tvGreeting);

        myObservable = Observable.just(greeting);
        myObservable.subscribeOn(Schedulers.io());

        myObservable.observeOn(AndroidSchedulers.mainThread());

        myObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String s) {
                Log.e(myApp, "onNext() Invoked");
                textView.setText(s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(myApp, "onError() Invoked");
            }

            @Override
            public void onComplete() {
                Log.e(myApp, "onComplete() Invoked");
            }
        };

        compositeDisposable.add(
                myObservable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(myObserver)
        );

        myObserver1 = new DisposableObserver<String>() {
            @Override
            public void onNext(String s) {
                Log.e(myApp, "onNext() Invoked");
                textView.setText(s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(myApp, "onError() Invoked");
            }

            @Override
            public void onComplete() {
                Log.e(myApp, "onComplete() Invoked");
            }
        };

        // compositeDisposable.add(myObserver1);
        //myObservable.subscribe(myObserver1);

        compositeDisposable.add(myObservable.subscribeWith(myObserver1));

        /*myObserver = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.e(myApp, "onSubscribe() Invoked");
                disposable = d;
            }

            @Override
            public void onNext(String s) {
                Log.e(myApp, "onNext() Invoked");
                textView.setText(s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(myApp, "onError() Invoked");
            }

            @Override
            public void onComplete() {
                Log.e(myApp, "onComplete() Invoked");
            }
        };*/


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(myApp, "onDestroy()");
//        myObserver.dispose();
//        myObserver1.dispose();

        compositeDisposable.clear();
    }
}
